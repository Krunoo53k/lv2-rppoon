﻿using System;
using System.Collections.Generic;

namespace LV2_RPPOON
{
    class Program
    {
        
        static void Main(string[] args)
        {

            DiceRoller diceRoller = new DiceRoller();
            for(int i=0;i<20;i++)
            {
                Die die = new Die(6);
                diceRoller.InsertDie(die);
            }

            diceRoller.RollAllDice();

            foreach (var element in diceRoller.GetRollingResults())
            {
                Console.WriteLine(element);
            }
        }
    }
}
